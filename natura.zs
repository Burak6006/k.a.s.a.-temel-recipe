import minetweaker.item.IItemStack;
import mods.gregtech.Assembler;
import mods.gregtech.CuttingSaw;
import mods.gregtech.Lathe;
furnace.remove(<Natura:NetherGlass>);

// arraylar //plank yerine log yazmışım
var logs = [<Natura:planks>,<Natura:planks:1>,<Natura:planks:2>,<Natura:planks:3>,<Natura:planks:4>,<Natura:planks:5>,<Natura:planks:6>,<Natura:planks:7>,<Natura:planks:8>,<Natura:planks:9>,<Natura:planks:10>,<Natura:planks:11>,<Natura:planks:12>] as IItemStack[];
var planks = [<Natura:tree>,<Natura:tree:1>,<Natura:tree:2>,<Natura:redwood:1>,<Natura:bloodwood>,<Natura:tree:3>,<Natura:Rare Tree>,<Natura:Rare Tree:1>,<Natura:Rare Tree:2>,<Natura:Rare Tree:3>,<Natura:willow>,<Natura:Dark Tree>,<Natura:Dark Tree:1>] as IItemStack[];
var doors = [<Natura:redwoodDoorItem:1>,<Natura:redwoodDoorItem:3>,<Natura:redwoodDoorItem:4>,<Natura:redwoodDoorItem>,<Natura:redwoodDoorItem:5>,<Natura:redwoodDoorItem:2>] as IItemStack[];
var sticks = [<Natura:natura.stick>,<Natura:natura.stick:1>,<Natura:natura.stick:2>,<Natura:natura.stick:3>,<Natura:natura.stick:4>,<Natura:natura.stick:5>,<Natura:natura.stick:6>,<Natura:natura.stick:7>,<Natura:natura.stick:8>,<Natura:natura.stick:9>,<Natura:natura.stick:10>,<Natura:natura.stick:11>,<Natura:natura.stick:12>] as IItemStack[];
var pressure_plates = [<Natura:pressureplate.eucalyptus>,<Natura:pressureplate.sakura>,<Natura:pressureplate.ghostwood>,<Natura:pressureplate.redwood>,<Natura:pressureplate.bloodwood>,<Natura:pressureplate.hopseed>,<Natura:pressureplate.maple>,<Natura:pressureplate.silverbell>,<Natura:pressureplate.amaranth>,<Natura:pressureplate.tiger>,<Natura:pressureplate.willow>,<Natura:pressureplate.darkwood>,<Natura:pressureplate.fusewood>] as IItemStack[];
var slabs = [<Natura:plankSlab1>,<Natura:plankSlab1:1>,<Natura:plankSlab1:2>,<Natura:plankSlab1:3>,<Natura:plankSlab1:4>,<Natura:plankSlab1:5>,<Natura:plankSlab1:6>,<Natura:plankSlab1:7>,<Natura:plankSlab2>,<Natura:plankSlab2:1>,<Natura:plankSlab2:2>,<Natura:plankSlab2:3>,<Natura:plankSlab2:4>] as IItemStack[];
var buttons = [<Natura:button.eucalyptus>,<Natura:button.sakura>,<Natura:button.ghostwood>,<Natura:button.redwood>,<Natura:button.bloodwood>,<Natura:button.hopseed>,<Natura:button.maple>,<Natura:button.silverbell>,<Natura:button.amaranth>,<Natura:button.tiger>,<Natura:button.willow>,<Natura:button.darkwood>,<Natura:button.fusewood>] as IItemStack[];
var items = [<Natura:natura.sword.ghostwood>,<Natura:natura.pickaxe.ghostwood>,<Natura:natura.shovel.ghostwood>,<Natura:natura.axe.ghostwood>,<Natura:natura.sword.bloodwood>,<Natura:natura.pickaxe.bloodwood>,<Natura:natura.shovel.bloodwood>,<Natura:natura.axe.bloodwood>,<Natura:natura.sword.darkwood>,<Natura:natura.pickaxe.darkwood>,<Natura:natura.shovel.darkwood>,<Natura:natura.sword.fusewood>,<Natura:natura.axe.darkwood>,<Natura:natura.pickaxe.fusewood>,<Natura:natura.kama.darkwood>,<Natura:natura.kama.bloodwood>,<Natura:natura.kama.fusewood>,<Natura:natura.shovel.netherquartz>,<Natura:natura.axe.netherquartz>,<Natura:natura.kama.netherquartz>,<Natura:natura.kama.ghostwood>,<Natura:natura.axe.fusewood>,<Natura:natura.shovel.fusewood>,<Natura:natura.sword.netherquartz>,<Natura:natura.pickaxe.netherquartz>] as IItemStack[];

// üretim döngüleri
for i, log in logs{
  //recipe removes
  recipes.remove(buttons[i]);

  recipes.remove(logs[i]);

  recipes.remove(pressure_plates[i]);

  recipes.remove(slabs[i]);

  recipes.remove(sticks[i]);

  //pressure plates
  Assembler.addRecipe(pressure_plates[i], log * 2, <gregtech:gt.integrated_circuit:2> * 0 ,80, 4);

  //slabs
  CuttingSaw.addRecipe([slabs[i] * 2], log, <liquid:water> * 4, 50, 4);

  CuttingSaw.addRecipe([slabs[i] * 2], log, <liquid:ic2distilledwater> * 3, 50, 4);

  CuttingSaw.addRecipe([slabs[i] * 2], log, <liquid:lubricant> * 1, 25, 4);

  recipes.addShaped(slabs[i]*2,
    [
      [logs[i],<ore:craftingToolSaw>]
    ]
  );

  //planks
  CuttingSaw.addRecipe([log * 4,<gregtech:gt.metaitem.01:2809>*2], planks[i], <liquid:water> * 5, 400, 8);

  CuttingSaw.addRecipe([log * 4,<gregtech:gt.metaitem.01:2809>*2], planks[i], <liquid:ic2distilledwater> * 3, 400, 8);

  CuttingSaw.addRecipe([log * 6,<gregtech:gt.metaitem.01:2809>], planks[i], <liquid:lubricant> * 1, 200, 8);

  recipes.addShapeless(log*2,[planks[i]]);

  recipes.addShaped(log*4,
    [
      [<ore:craftingToolSaw>],
      [     planks[i]       ]
    ]
  );
  //sticks
  Lathe.addRecipe([sticks[i] * 2], log, 10, 8);

  recipes.addShaped(sticks[i]*4,
    [
      [<ore:craftingToolSaw>],
      [         log         ],
      [         log         ]
    ]
  );

  recipes.addShapedMirrored(sticks[i] * 2,
    [
      [log],
      [log]
    ]
  );
}


for i,item in items{
  recipes.remove(item);
  item.addTooltip(format.red("Ha ha bu eşyaları kullanamazsın"));
}


//recipe removes
recipes.remove(<Natura:Blazerail>);
recipes.remove(<Natura:BrailPowered>);
recipes.remove(<Natura:BrailDetector>);
recipes.remove(<Natura:BrailActivator>);
recipes.remove(<Natura:NetherPressurePlate>);
recipes.remove(<Natura:NetherButton>);
recipes.remove(<Natura:barleyFood:2>);
recipes.remove(<Natura:NetherHopper>);
furnace.remove(<*>, <Natura:barleyFood:1>);


//recipe adds
//dough from barley unu
recipes.addShapeless(<gregtech:gt.metaitem.02:32559>,[<Natura:barleyFood:1>,<ore:bucketWater>]);
//barley unu
recipes.remove(<Natura:barleyFood:1>);
recipes.addShaped(<Natura:barleyFood:1>, [[<Natura:barleyFood>],[<ore:craftingToolMortar>]]);


//assembler üretimleri
//nether pressure plate
Assembler.addRecipe(<Natura:NetherPressurePlate>, <minecraft:netherrack> * 2, <gregtech:gt.integrated_circuit:2> * 0 ,120, 16);
