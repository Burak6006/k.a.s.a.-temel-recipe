import minetweaker.item.IItemStack;
import mods.gregtech.Assembler;
import mods.gregtech.Compressor;
import mods.railcraft.Rolling;
import mods.gregtech.ChemicalBath;
Assembler.addRecipe(<StevesCarts:BlockAdvDetector>*8, [<gregtech:gt.metaitem.01:17308> *6, <gregtech:gt.metaitem.01:17308>, <gregtech:gt.metaitem.01:17308>], null, 80, 4);
//removes
var remove = [<Railcraft:machine.alpha:1>,<Railcraft:machine.beta:6>,<Railcraft:machine.beta:5>,<Railcraft:machine.beta:6>,<Railcraft:machine.beta:5>,<Railcraft:track:2264>,<Railcraft:machine.epsilon:5>,<Railcraft:machine.epsilon:3>,<Railcraft:machine.beta:3>,<Railcraft:machine.beta:4>,<Railcraft:part.turbine.blade>,<Railcraft:part.turbine.disk>,<Railcraft:part.turbine.rotor>,<Railcraft:machine.alpha>,<Railcraft:machine.alpha:2>,<Railcraft:machine.alpha:13>,<Railcraft:machine.alpha:11>,<Railcraft:machine.alpha:3>,<Railcraft:machine.beta:13>,<Railcraft:machine.gamma:6>,<Railcraft:machine.gamma:7>,<Railcraft:machine.beta:14>,<Railcraft:machine.beta:15>,<Railcraft:detector>,<Railcraft:detector:1>,<Railcraft:detector:2>,<Railcraft:detector:3>,<Railcraft:detector:4>,<Railcraft:detector:5>,<Railcraft:detector:6>,<Railcraft:detector:7>,<Railcraft:detector:8>,<Railcraft:detector:9>,<Railcraft:detector:10>,<Railcraft:detector:11>,<Railcraft:detector:12>,<Railcraft:detector:13>,<Railcraft:detector:14>,<Railcraft:detector:15>,<Railcraft:detector:16>,<Railcraft:machine.gamma:11>,<Railcraft:machine.gamma:10>,<Railcraft:detector:10>,<Railcraft:machine.gamma:5>,<Railcraft:machine.gamma:5>,<Railcraft:machine.gamma:4>,<Railcraft:machine.gamma:4>,<Railcraft:machine.gamma:3>,<Railcraft:machine.gamma:2>,<Railcraft:machine.gamma:1>,<Railcraft:machine.gamma>,<Railcraft:machine.alpha:14>,<Railcraft:machine.beta:12>,<Railcraft:machine.beta:7>,<Railcraft:machine.beta:8>,<Railcraft:machine.beta:9>,<Railcraft:machine.alpha:6>,<Railcraft:frame>,<Railcraft:machine.delta>,<Railcraft:cart.loco.electric>,<Railcraft:machine.alpha:12>,<Railcraft:machine.alpha:15>,<Railcraft:part.gear:3>,<Railcraft:part.gear:2>,<Railcraft:part.gear:1>,<Railcraft:part.gear>] as IItemStack[];
for i,rem in remove{
  recipes.remove(rem);
}
recipes.removeShaped(<Railcraft:machine.beta>.withTag({color: 15 as byte}), [[<Railcraft:part.plate>, <Railcraft:part.plate>], [<Railcraft:part.plate>, <Railcraft:part.plate>]]);
recipes.removeShaped(<Railcraft:machine.beta:1>.withTag({color: 15 as byte}), [[<ore:paneGlassColorless>, <Railcraft:part.plate>, <ore:paneGlassColorless>], [<Railcraft:part.plate>, <ore:paneGlassColorless>, <Railcraft:part.plate>], [<ore:paneGlassColorless>, <Railcraft:part.plate>, <ore:paneGlassColorless>]]);
recipes.removeShaped(<Railcraft:machine.beta:2>.withTag({color: 15 as byte}), [[<minecraft:iron_bars>, <Railcraft:part.plate>, <minecraft:iron_bars>], [<Railcraft:part.plate>, <minecraft:lever>, <Railcraft:part.plate>], [<minecraft:iron_bars>, <Railcraft:part.plate>, <minecraft:iron_bars>]]);
recipes.removeShaped(<Railcraft:machine.beta>, [[null, <gregtech:gt.metaitem.01:17032>, null], [<gregtech:gt.metaitem.01:17032>, <ore:craftingToolHardHammer>, <gregtech:gt.metaitem.01:17032>], [null, <gregtech:gt.metaitem.01:17032>, null]]);
Rolling.removeRecipe(<Railcraft:part.plate>);
Rolling.removeRecipe(<Railcraft:part.plate:1>);
Rolling.removeRecipe(<Railcraft:part.plate:2>);
Rolling.removeRecipe(<Railcraft:part.plate:3>);
Rolling.removeRecipe(<Railcraft:part.plate:4>);


//adds
//sentinel
recipes.remove(<Railcraft:machine.beta:10>);
recipes.addShaped(<Railcraft:machine.beta:10>, [[null, <ore:plateEnderPearl>, null], [null, <ore:stoneObsidian>, null], [<ore:stoneObsidian>, <ore:plateGold>, <ore:stoneObsidian>]]);

//steel anvil
recipes.remove(<Railcraft:anvil>);
recipes.addShaped(<Railcraft:anvil>, [[<ore:blockSteel>, <ore:blockSteel>, <ore:blockSteel>], [<ore:screwSteel>, <ore:plateSteel>, <ore:screwSteel>], [<ore:plateSteel>, <ore:plateSteel>, <ore:plateSteel>]]);

//creosite wood
recipes.remove(<Railcraft:cube:8>);
ChemicalBath.addRecipe([<Railcraft:cube:8>],<ore:logWood>,<liquid:creosote>*750,[10000],80,28);

//coke block
recipes.remove(<Railcraft:cube>);
Compressor.addRecipe(<Railcraft:cube>,<Railcraft:fuel.coke>*9,160,30);

//liquid firebox
recipes.addShaped(<Railcraft:machine.beta:6>,
  [
    [<ore:plateSteel>, <gregtech:gt.blockmachines:5133>, <ore:plateSteel>],
    [<minecraft:iron_bars:*>, <minecraft:fire_charge:*>, <minecraft:iron_bars:*>],
    [<ore:plateSteel>, <IC2:blockMachine:1>, <ore:plateSteel>]
  ]
);

//solid firebox
recipes.addShaped(<Railcraft:machine.beta:5>,
  [
    [<ore:plateSteel>, <ore:bucketEmpty>, <ore:plateSteel>],
    [<minecraft:iron_bars:*>, <minecraft:fire_charge:*>, <minecraft:iron_bars:*>],
    [<ore:plateSteel>, <IC2:blockMachine:1>, <ore:plateSteel>]
  ]
);

//disposal track
recipes.addShaped(<Railcraft:track:2264>,
  [
    [<Railcraft:part.rail>, <Railcraft:part.tie>, <Railcraft:part.rail>],
    [<Railcraft:part.rail>, <gregtech:gt.metaitem.01:17305>, <Railcraft:part.rail>],
    [<Railcraft:part.rail>, <Railcraft:part.tie>, <Railcraft:part.rail>]
  ]
);

//engraving bench
recipes.addShaped(<Railcraft:machine.epsilon:5>,
  [
    [<minecraft:diamond_pickaxe>, <gregtech:gt.metaitem.01:17305>, <minecraft:book>],
    [<gregtech:gt.metaitem.01:17305>, <minecraft:crafting_table:*>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:piston:*>, <gregtech:gt.metaitem.01:17305>, <minecraft:piston:*>]
  ]
);

//force track emiiter
recipes.addShaped(<Railcraft:machine.epsilon:3>,
  [
    [<gregtech:gt.metaitem.01:17032>, <ore:ingotCopper>, <gregtech:gt.metaitem.01:17032>],
    [<ore:ingotCopper>, <ore:blockDiamond>, <ore:ingotCopper>],
    [<gregtech:gt.metaitem.01:17032>, <ore:ingotCopper>, <gregtech:gt.metaitem.01:17032>]
  ]
);

//lp boiler tank
recipes.addShaped(<Railcraft:machine.beta:3>,
  [
    [<gregtech:gt.metaitem.01:17032>],
    [<ore:craftingToolHardHammer>],
    [<gregtech:gt.metaitem.01:17032>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:3>, <gregtech:gt.metaitem.01:17032>, <gregtech:gt.integrated_circuit:1> * 0 ,480, 24);

//hp boiler tank
recipes.addShaped(<Railcraft:machine.beta:4>,
  [
    [<gregtech:gt.metaitem.01:17305>],
    [<ore:craftingToolHardHammer>],
    [<gregtech:gt.metaitem.01:17305>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:4>, <gregtech:gt.metaitem.01:17305>, <gregtech:gt.integrated_circuit:1> * 0 ,480, 24);

//world anchor
recipes.addShaped(<Railcraft:machine.alpha>,
  [
    [<gregtech:gt.metaitem.01:17086>, <minecraft:obsidian>, <gregtech:gt.metaitem.01:17086>],
    [<gregtech:gt.metaitem.01:17500>, <minecraft:ender_pearl>, <gregtech:gt.metaitem.01:17500>],
    [<gregtech:gt.metaitem.01:17086>, <minecraft:obsidian>, <gregtech:gt.metaitem.01:17086>]
  ]
);

//personal anchor
recipes.addShaped(<Railcraft:machine.alpha:2>,
  [
    [<gregtech:gt.metaitem.01:17086>, <minecraft:obsidian>, <gregtech:gt.metaitem.01:17086>],
    [<gregtech:gt.metaitem.01:17501>, <minecraft:ender_pearl>, <gregtech:gt.metaitem.01:17501>],
    [<gregtech:gt.metaitem.01:17086>, <minecraft:obsidian>, <gregtech:gt.metaitem.01:17086>]
  ]
);

//passive anchor
recipes.addShaped(<Railcraft:machine.alpha:13>,
  [
    [<gregtech:gt.metaitem.01:17086>, <minecraft:obsidian>, <gregtech:gt.metaitem.01:17086>],
    [<gregtech:gt.metaitem.01:17526>, <minecraft:ender_pearl>, <gregtech:gt.metaitem.01:17526>],
    [<gregtech:gt.metaitem.01:17086>, <minecraft:obsidian>, <gregtech:gt.metaitem.01:17086>]
  ]
);


//electric locomotive
recipes.addShaped(<Railcraft:cart.loco.electric>.withTag({primaryColor: 11 as byte, secondaryColor: 0 as byte}),
  [
    [<minecraft:redstone_lamp:*>, <gregtech:gt.metaitem.01:17305>, null],
    [<gregtech:gt.metaitem.01:17305>, <Railcraft:machine.epsilon>, <gregtech:gt.metaitem.01:17305>],
    [<ore:gearGtSteel>, <minecraft:minecart>, <ore:gearGtSteel>]
  ]
);

//iron tank wall
recipes.addShaped(<Railcraft:machine.beta>*4,
  [
    [null,<gregtech:gt.metaitem.01:17032>,null],
    [<gregtech:gt.metaitem.01:17032>,<ore:craftingToolHardHammer>,<gregtech:gt.metaitem.01:17032>],
    [null,<gregtech:gt.metaitem.01:17032>,null]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta>*6, <gregtech:gt.metaitem.01:17032>*4, <gregtech:gt.integrated_circuit:4> * 0 ,240, 16);

//iron tank wall ama camlı
recipes.addShaped(<Railcraft:machine.beta:1>*4,
  [
    [<ore:paneGlassColorless>, <Railcraft:machine.beta>, <ore:paneGlassColorless>],
    [<Railcraft:machine.beta>, <ore:craftingToolHardHammer>, <Railcraft:machine.beta>],
    [<ore:paneGlassColorless>, <Railcraft:machine.beta>, <ore:paneGlassColorless>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:1>*4, [<ore:paneGlassColorless>*4,<gregtech:gt.metaitem.01:17032>*4, <gregtech:gt.integrated_circuit:4> * 0 ],null,160, 16);
Assembler.addRecipe(<Railcraft:machine.beta:1>, [<ore:paneGlassColorless>,<Railcraft:machine.beta>, <gregtech:gt.integrated_circuit:1> * 0 ],null,40, 16);

//iron tank valve
recipes.addShaped(<Railcraft:machine.beta:2>,
  [
    [<minecraft:iron_bars>, <gregtech:gt.metaitem.01:17032>, <minecraft:iron_bars>],
    [<gregtech:gt.metaitem.01:17032>, <gregtech:gt.blockmachines:5123>, <gregtech:gt.metaitem.01:17032>],
    [<minecraft:iron_bars>, <gregtech:gt.metaitem.01:17032>, <minecraft:iron_bars>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:2>, [<gregtech:gt.metaitem.01:17032>*4,<minecraft:iron_bars>*4,<gregtech:gt.blockmachines:5123>, <gregtech:gt.integrated_circuit:4> * 0 ],null,240, 16);

//water tank siding
recipes.addShaped(<Railcraft:machine.alpha:14>*2,
  [
    [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>],
    [<ore:stickIron>, <IC2:itemHarz>, <ore:stickIron>],
    [<ore:plankWood>, <ore:plankWood>, <ore:plankWood>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.alpha:14>*3,<ore:plankWood>*6,<IC2:itemHarz>,480, 24);

//steam oven
recipes.addShaped(<Railcraft:machine.alpha:3>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.metaitem.01:17305>, <gregtech:gt.metaitem.01:17305>],
    [<gregtech:gt.metaitem.01:17305>, <IC2:blockMachine:1>, <gregtech:gt.metaitem.01:17305>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.metaitem.01:17305>, <gregtech:gt.metaitem.01:17305>]
  ]
);

//feed station
recipes.addShaped(<Railcraft:machine.alpha:11>,
  [
    [<ore:plankWood>, <minecraft:golden_carrot>, <ore:plankWood>],
    [<minecraft:golden_carrot>, <gregtech:gt.metaitem.01:17305>, <minecraft:golden_carrot>],
    [<ore:plankWood>, <minecraft:golden_carrot>, <ore:plankWood>]
  ]
);


//item loader
recipes.addShaped(<Railcraft:machine.gamma>,
  [
    [<gregtech:gt.blockmachines:2006>, <minecraft:hopper>, <gregtech:gt.blockmachines:2006>],
    [<ore:cobblestone>, <gregtech:gt.blockmachines:11>, <ore:cobblestone>],
    [<gregtech:gt.blockmachines:2006>, <Railcraft:detector>, <gregtech:gt.blockmachines:2006>]
  ]
);

//item unloader
recipes.addShaped(<Railcraft:machine.gamma:1>,
  [
    [<gregtech:gt.blockmachines:2006>, <Railcraft:detector>, <gregtech:gt.blockmachines:2006>],
    [<ore:cobblestone>, <gregtech:gt.blockmachines:11>, <ore:cobblestone>],
    [<gregtech:gt.blockmachines:2006>, <minecraft:hopper>, <gregtech:gt.blockmachines:2006>]
  ]
);

//adv. item loader
recipes.addShaped(<Railcraft:machine.gamma:2>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:1246>, <gregtech:gt.metaitem.01:17305>],
    [<gregtech:gt.blockmachines:1246>, <Railcraft:machine.gamma>, <gregtech:gt.blockmachines:1246>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.metaitem.01:32640>, <gregtech:gt.metaitem.01:17305>]
  ]
);

//adv. item loader
recipes.addShaped(<Railcraft:machine.gamma:3>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:1246>, <gregtech:gt.metaitem.01:17305>],
    [<gregtech:gt.blockmachines:1246>, <Railcraft:machine.gamma:1>, <gregtech:gt.blockmachines:1246>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.metaitem.01:32640>, <gregtech:gt.metaitem.01:17305>]
  ]
);

//fluid loader
recipes.addShaped(<Railcraft:machine.gamma:4>,
  [
    [<gregtech:gt.blockmachines:1366>, <gregtech:gt.metaitem.01:32611>, <gregtech:gt.blockmachines:1366>],
    [<ore:blockGlassColorless>, <gregtech:gt.blockmachines:12>, <ore:blockGlassColorless>],
    [<minecraft:hopper:*>, <Railcraft:detector:8>, <minecraft:hopper:*>]
  ]
);

//fluid unloader
recipes.addShaped(<Railcraft:machine.gamma:5>,
  [
    [<gregtech:gt.blockmachines:1366>, <Railcraft:detector:8>, <gregtech:gt.blockmachines:1366>],
    [<ore:blockGlassColorless>, <gregtech:gt.blockmachines:12>, <ore:blockGlassColorless>],
    [<minecraft:hopper:*>, <gregtech:gt.metaitem.01:32611>, <minecraft:hopper:*>]
  ]
);

//energy loader
recipes.addShaped(<Railcraft:machine.gamma:6>,
  [
    [<gregtech:gt.metaitem.01:22089>, <gregtech:gt.blockmachines:171>,<gregtech:gt.metaitem.01:22089>],
    [<IC2:itemBatREDischarged>.withTag({}), <gregtech:gt.metaitem.01:17810>, <IC2:itemBatREDischarged>.withTag({})],
    [<IC2:itemBatREDischarged>.withTag({}), <Railcraft:detector:9>, <IC2:itemBatREDischarged>.withTag({})]
  ]
);

//energy unloader
recipes.addShaped(<Railcraft:machine.gamma:7>,
  [
    [<gregtech:gt.metaitem.01:22089>, <Railcraft:detector:9>,<gregtech:gt.metaitem.01:22089>],
    [<IC2:itemBatREDischarged>.withTag({}), <gregtech:gt.metaitem.01:17810>, <IC2:itemBatREDischarged>.withTag({})],
    [<IC2:itemBatREDischarged>.withTag({}), <gregtech:gt.blockmachines:171>, <IC2:itemBatREDischarged>.withTag({})]
  ]
);

recipes.addShaped(<Railcraft:machine.beta:13>*4,
  [
    [null,<gregtech:gt.metaitem.01:17305>,null],
    [<gregtech:gt.metaitem.01:17305>,<ore:craftingToolHardHammer>,<gregtech:gt.metaitem.01:17305>],
    [null,<gregtech:gt.metaitem.01:17305>,null]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:13>*6, <gregtech:gt.metaitem.01:17305>*4, <gregtech:gt.integrated_circuit:4> * 0 ,240, 48);

recipes.addShaped(<Railcraft:machine.beta:14>*4,
  [
    [<ore:paneGlassColorless>, <Railcraft:machine.beta:13>, <ore:paneGlassColorless>],
    [<Railcraft:machine.beta:13>, <ore:craftingToolHardHammer>, <Railcraft:machine.beta:13>],
    [<ore:paneGlassColorless>, <Railcraft:machine.beta:13>, <ore:paneGlassColorless>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:14>*4, [<ore:paneGlassColorless>*4,<gregtech:gt.metaitem.01:17305>*4,<gregtech:gt.integrated_circuit:4> * 0],null,240, 36);
Assembler.addRecipe(<Railcraft:machine.beta:14>, [<ore:paneGlassColorless>,<Railcraft:machine.beta>, <gregtech:gt.integrated_circuit:1> * 0],null ,60, 36);

recipes.addShaped(<Railcraft:machine.beta:15>,
  [
    [<minecraft:iron_bars>, <gregtech:gt.metaitem.01:17305>, <minecraft:iron_bars>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:5134>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:iron_bars>, <gregtech:gt.metaitem.01:17305>, <minecraft:iron_bars>]
  ]
);
Assembler.addRecipe(<Railcraft:machine.beta:15>, [<gregtech:gt.metaitem.01:17305>*4,<minecraft:iron_bars>*4,<gregtech:gt.blockmachines:5134>, <gregtech:gt.integrated_circuit:4>*0],null ,360, 48);





//sensors
//sensors
//sensors
//sensors
//sensors
//sensors
//sensors
//sensors

recipes.addShaped(<Railcraft:detector>,
  [
    [<gregtech:gt.metaitem.01:17304>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17304>],
    [<gregtech:gt.blockmachines:2006>, <ore:plankWood>, <gregtech:gt.blockmachines:2006>],
    [<gregtech:gt.metaitem.01:17304>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17304>]
  ]
);

recipes.addShaped(<Railcraft:detector:1>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<ore:stone>, <Railcraft:detector>, <ore:stone>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:2>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:stonebrick>, <Railcraft:detector:1>, <minecraft:stonebrick>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:3>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:stonebrick:1>, <Railcraft:detector:1>, <minecraft:stonebrick:1>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:3>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:mossy_cobblestone:*>, <Railcraft:detector:1>, <minecraft:mossy_cobblestone:*>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:4>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<ore:cobblestone>, <Railcraft:detector:1>, <ore:cobblestone>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:5>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:stone_slab>, <Railcraft:detector:1>, <minecraft:stone_slab>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:6>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<ore:slabWood>, <Railcraft:detector:1>, <ore:slabWood>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:7>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:log>, <Railcraft:detector:1>, <minecraft:log>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:8>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<ore:ingotBrick>, <Railcraft:detector:1>, <ore:ingotBrick>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:9>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<gregtech:gt.blockmachines:1246>, <Railcraft:detector:1>, <gregtech:gt.blockmachines:1246>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:11>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:log:1>, <Railcraft:detector:1>, <minecraft:log:1>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:12>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:nether_brick:*>, <Railcraft:detector:1>, <minecraft:nether_brick:*>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:13>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:wool:*>, <Railcraft:detector:1>, <minecraft:wool:*>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:14>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:leather>, <Railcraft:detector:1>, <minecraft:leather>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:15>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<Railcraft:brick.infernal>, <Railcraft:detector:1>, <Railcraft:brick.infernal>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);

recipes.addShaped(<Railcraft:detector:16>,
  [
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>],
    [<minecraft:quartz_block:1>, <Railcraft:detector:1>, <minecraft:quartz_block:1>],
    [<gregtech:gt.metaitem.01:17305>, <gregtech:gt.blockmachines:2006>, <gregtech:gt.metaitem.01:17305>]
  ]
);
