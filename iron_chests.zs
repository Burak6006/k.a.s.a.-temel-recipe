import mods.gregtech.Assembler;
//adds
recipes.remove(<IronChest:BlockIronChest>);
recipes.addShaped(<IronChest:BlockIronChest>,
  [
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>],
    [<ore:plateIron>, <ore:chestWood>, <ore:plateIron>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
  ]
);

recipes.remove(<IronChest:BlockIronChest:1>);
recipes.addShaped(<IronChest:BlockIronChest:1>,
  [
    [<ore:plateGold>,     <ore:plateGold>       , <ore:plateGold>],
    [<ore:plateGold>, <IronChest:BlockIronChest>, <ore:plateGold>],
    [<ore:plateGold>,     <ore:plateGold>       , <ore:plateGold>]
  ]
);

recipes.remove(<IronChest:BlockIronChest:2>);
recipes.addShaped(<IronChest:BlockIronChest:2>,
  [
    [<ore:blockGlass>  ,       <ore:blockGlass>      , <ore:blockGlass>  ],
    [<ore:plateDiamond>, <IronChest:BlockIronChest:1>, <ore:plateDiamond>],
    [<ore:blockGlass>  ,      <ore:blockGlass>       , <ore:blockGlass>  ]
  ]
);

recipes.remove(<IronChest:BlockIronChest:3>);
recipes.addShaped(<IronChest:BlockIronChest:3>,
  [
    [<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>],
    [<ore:plateCopper>, <ore:chestWood>, <ore:plateCopper>],
    [<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>]
  ]
);

recipes.remove(<IronChest:BlockIronChest:4>);
recipes.addShaped(<IronChest:BlockIronChest:4>,
  [
    [<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>],
    [<ore:plateSilver>, <IronChest:BlockIronChest:3>, <ore:plateSilver>],
    [<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>]
  ]
);

recipes.remove(<IronChest:ironGoldUpgrade>);
recipes.addShaped(<IronChest:ironGoldUpgrade>,
  [
    [<ore:plateGold>, <ore:plateGold>, <ore:plateGold>],
    [<ore:plateGold>, <ore:plateIron>, <ore:plateGold>],
    [<ore:plateGold>, <ore:plateGold>, <ore:plateGold>]
  ]
);

recipes.remove(<IronChest:goldDiamondUpgrade>);
recipes.addShaped(<IronChest:goldDiamondUpgrade>,
  [
    [<minecraft:glass:*>, <minecraft:glass:*>, <minecraft:glass:*>],
    [<ore:plateDiamond>, <ore:plateGold>, <ore:plateDiamond>],
    [<minecraft:glass:*>, <minecraft:glass:*>, <minecraft:glass:*>]
  ]
);

recipes.remove(<IronChest:copperSilverUpgrade>);
recipes.addShaped(<IronChest:copperSilverUpgrade>,
  [
    [<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>],
    [<ore:plateSilver>, <ore:plateCopper>, <ore:plateSilver>],
    [<ore:plateSilver>, <ore:plateSilver>, <ore:plateSilver>]
  ]
);

recipes.remove(<IronChest:silverGoldUpgrade>);
recipes.addShaped(<IronChest:silverGoldUpgrade>,
  [
    [<ore:plateGold>, <minecraft:glass:*>, <ore:plateGold>],
    [<minecraft:glass:*>, <ore:plateSilver>, <minecraft:glass:*>],
    [<ore:plateGold>, <minecraft:glass:*>, <ore:plateGold>]
  ]
);

recipes.remove(<IronChest:copperIronUpgrade>);
recipes.addShaped(<IronChest:copperIronUpgrade>,
  [
    [<ore:plateIron>, <minecraft:glass:*>, <ore:plateIron>],
    [<minecraft:glass:*>, <ore:plateCopper>, <minecraft:glass:*>],
    [<ore:plateIron>, <minecraft:glass:*>, <ore:plateIron>]
  ]
);

recipes.remove(<IronChest:woodIronUpgrade>);
recipes.addShaped(<IronChest:woodIronUpgrade>,
  [
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>],
    [<ore:plateIron>, <ore:plankWood>, <ore:plateIron>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
  ]
);

recipes.remove(<IronChest:woodCopperUpgrade>);
recipes.addShaped(<IronChest:woodCopperUpgrade>,
  [
    [<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>],
    [<ore:plateCopper>, <ore:plankWood>, <ore:plateCopper>],
    [<ore:plateCopper>, <ore:plateCopper>, <ore:plateCopper>]
  ]
);
