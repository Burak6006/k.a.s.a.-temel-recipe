//iron saw

recipes.remove(<ForgeMicroblock:sawIron>);

recipes.addShaped(<ForgeMicroblock:sawIron>,
  [
    [<ore:stickWood>, <ore:stickIron>, <ore:stickIron>],
    [<ore:stickWood>, <ore:toolHeadSawIron>, <ore:stickIron>]
  ]
);

//dia saw

recipes.remove(<ForgeMicroblock:sawDiamond>);

recipes.addShaped(<ForgeMicroblock:sawDiamond>,
  [
    [<ore:stickWood>, <ore:stickIron>,<ore:stickIron>],
    [<ore:stickWood>,<minecraft:diamond>, <ore:stickIron>],
    [<ore:craftingToolHardHammer>,null,<ore:craftingToolFile>]
  ]
);
