import mods.gregtech.Compressor;

//tuning fork

recipes.remove(<JABBA:tuningFork>);

recipes.addShaped(<JABBA:tuningFork>,
  [
    [<ore:plateIron>, <ore:screwSteel>, <ore:craftingToolScrewdriver>],
    [<ore:screwSteel>, <appliedenergistics2:item.ToolMemoryCard>, <ore:screwSteel>],
    [<ore:plateIron>, <ore:screwSteel>, <ore:plateIron>]
  ]
);

//redstone facade

recipes.remove(<JABBA:upgradeSide:2>);

recipes.addShaped(<JABBA:upgradeSide:2>,
  [
    [<ore:plateIron>, <ore:plateWood>, <ore:plateIron>],
    [<ore:plateWood>, <minecraft:redstone>, <ore:plateWood>],
    [<ore:plateIron>, <ore:plateWood>, <ore:plateIron>]
  ]
);

//redstone

recipes.remove(<JABBA:upgradeCore:2>);

recipes.addShaped(<JABBA:upgradeCore:2>,
  [
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>],
    [<ore:plateIron>, <minecraft:redstone_block:*>, <ore:plateIron>],
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>]
  ]
);

//bspace

recipes.remove(<JABBA:upgradeCore:1>);

recipes.addShaped(<JABBA:upgradeCore:1>,
  [
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>],
    [<ore:plateIron>, <ore:transdimBlock>, <ore:plateIron>],
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>]
  ]
);

//void

recipes.remove(<JABBA:upgradeCore:7>);

recipes.addShaped(<JABBA:upgradeCore:7>,
  [
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>],
    [<ore:plateIron>, <minecraft:obsidian:*>, <ore:plateIron>],
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>]
  ]
);

//hopper

recipes.remove(<JABBA:upgradeCore:3>);

recipes.addShaped(<JABBA:upgradeCore:3>,
  [
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>],
    [<ore:plateIron>, <minecraft:hopper:*>, <ore:plateIron>],
    [<ore:plateWood>, <minecraft:piston:*>, <ore:plateWood>]
  ]
);

//upgrades

recipes.remove(<JABBA:upgradeCore:4>);
recipes.remove(<JABBA:upgradeCore:5>);
recipes.remove(<JABBA:upgradeCore:6>);
recipes.remove(<JABBA:upgradeCore:8>);
recipes.remove(<JABBA:upgradeCore:9>);

Compressor.addRecipe(<JABBA:upgradeCore:4>,<JABBA:upgradeCore>*3,320,8);
Compressor.addRecipe(<JABBA:upgradeCore:5>,<JABBA:upgradeCore:4>*3,320,16);
Compressor.addRecipe(<JABBA:upgradeCore:6>,<JABBA:upgradeCore:5>*3,320,32);
Compressor.addRecipe(<JABBA:upgradeCore:8>,<JABBA:upgradeCore:6>*3,320,64);
Compressor.addRecipe(<JABBA:upgradeCore:9>,<JABBA:upgradeCore:8>*3,320,128);
//hammer

recipes.remove(<JABBA:hammer>);

recipes.addShaped(<JABBA:hammer>,
  [
    [<ore:plateIron>, <ore:ingotIron>, <ore:plateIron>],
    [<ore:craftingToolFile>, <ore:stickWood>, <ore:craftingToolHardHammer>],
    [null, <ore:stickWood>, null]
  ]
);

//

recipes.remove(<JABBA:upgradeStructural>);

recipes.addShaped(<JABBA:upgradeStructural>,
  [
    [<ore:plateWood>, <ore:plankWood>, <ore:plateWood>],
    [<ore:plankWood>, <JABBA:upgradeCore>, <ore:plankWood>],
    [<ore:plateWood>, <ore:plankWood>, <ore:plateWood>]
  ]
);

//

recipes.remove(<JABBA:upgradeStructural:1>);

recipes.addShaped(<JABBA:upgradeStructural:1>,
  [
    [<ore:plateWood>, <ore:plateIron>, <ore:plateWood>],
    [<ore:plateIron>, <JABBA:upgradeStructural>, <ore:plateIron>],
    [<ore:plateWood>, <ore:plateIron>, <ore:plateWood>]]);

//

recipes.remove(<JABBA:upgradeStructural:2>);

recipes.addShaped(<JABBA:upgradeStructural:2>,
  [
    [<ore:plateWood>, <ore:plateGold>, <ore:plateWood>],
    [<ore:plateGold>, <JABBA:upgradeStructural:1>, <ore:plateGold>],
    [<ore:plateWood>, <ore:plateGold>, <ore:plateWood>]
  ]
);

//

recipes.remove(<JABBA:upgradeStructural:3>);

recipes.addShaped(<JABBA:upgradeStructural:3>,
  [
    [<ore:plateWood>, <ore:plateDiamond>, <ore:plateWood>],
    [<ore:plateDiamond>, <JABBA:upgradeStructural:2>, <ore:plateDiamond>],
    [<ore:plateWood>, <ore:plateDiamond>, <ore:plateWood>]
  ]
);

//

recipes.remove(<JABBA:upgradeStructural:4>);

recipes.addShaped(<JABBA:upgradeStructural:4>,
  [
    [<ore:plateWood>, <ore:obsidian>, <ore:plateWood>],
    [<ore:obsidian>, <JABBA:upgradeStructural:3>, <ore:obsidian>],
    [<ore:plateWood>, <ore:obsidian>, <ore:plateWood>]
  ]
);

//

recipes.remove(<JABBA:upgradeStructural:5>);

recipes.addShaped(<JABBA:upgradeStructural:5>,
  [
    [<ore:plateWood>, <ore:whiteStone>, <ore:plateWood>],
    [<ore:whiteStone>, <JABBA:upgradeStructural:4>, <ore:whiteStone>],
    [<ore:plateWood>, <ore:whiteStone>, <ore:plateWood>]
  ]
);

//

recipes.remove(<JABBA:upgradeStructural:6>);

recipes.addShaped(<JABBA:upgradeStructural:6>,
  [
    [<ore:plateWood>, <ore:plateEmerald>, <ore:plateWood>],
    [<ore:plateEmerald>, <JABBA:upgradeStructural:5>, <ore:plateEmerald>],
    [<ore:plateWood>, <ore:plateEmerald>, <ore:plateWood>]
  ]
);
