//chisel

recipes.remove(<ArchitectureCraft:hammer>);

recipes.addShaped(<ArchitectureCraft:chisel>,
  [
    [<ore:plateIron>, <ore:craftingToolHardHammer>],
    [<minecraft:dye:14>, <minecraft:stick>]
  ]
);

//hammer

recipes.remove(<ArchitectureCraft:hammer>);

recipes.addShaped(<ArchitectureCraft:hammer>,
  [
    [<ore:plateIron>, <minecraft:iron_ingot>, <ore:craftingToolFile>],
    [<minecraft:dye:14>, <minecraft:stick>, <minecraft:iron_ingot>],
    [<minecraft:dye:14>, <minecraft:stick>, <ore:craftingToolHardHammer>]
  ]
);

//sawbench

recipes.remove(<ArchitectureCraft:sawbench>);

recipes.addShaped(<ArchitectureCraft:sawbench>,
  [
    [<ore:plateIron>, <ArchitectureCraft:sawblade>, <ore:plateIron>],
    [<minecraft:stick>, <ArchitectureCraft:largePulley>, <minecraft:stick>],
    [<minecraft:stick>, <minecraft:wooden_pressure_plate:*>, <minecraft:stick>]
  ]
);

//sawblade

recipes.remove(<ArchitectureCraft:sawblade>);

recipes.addShaped(<ArchitectureCraft:sawblade>,
  [
    [<ore:craftingToolHardHammer>, <ore:plateIron>, <ore:craftingToolWireCutter>],
    [<ore:plateIron>, <minecraft:stick>, <ore:plateIron>],
    [<ore:craftingToolWrench>, <ore:plateIron>, <ore:craftingToolFile>]
  ]
);

//largepulley

recipes.remove(<ArchitectureCraft:largePulley>);

recipes.addShaped(<ArchitectureCraft:largePulley>,
  [
    [<ore:screwIron>, <ore:plateWood>, <ore:craftingToolScrewdriver>],
    [<ore:plateWood>, <minecraft:stick>, <ore:plateWood>],
    [null, <ore:plateWood>, <ore:screwIron>]
  ]
);
