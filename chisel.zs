//dia

recipes.remove(<chisel:diamondChisel>);

recipes.addShaped(<chisel:diamondChisel>,
  [
    [<ore:plateDiamond>, <ore:craftingToolHardHammer>, null],
    [<ore:craftingToolFile>, <ore:stickWood>, null],
    [null, null, <ore:stickWood>]
  ]
);

//obs

recipes.remove(<chisel:obsidianChisel>);

recipes.addShaped(<chisel:obsidianChisel>,
  [
    [<ore:plateObsidian>, <ore:craftingToolHardHammer>, null],
    [<ore:craftingToolFile>, <ore:stickWood>, null],
    [null, null, <ore:stickWood>]
  ]
);
