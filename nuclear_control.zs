//Energy Counter
recipes.remove(<IC2NuclearControl:blockNuclearControlMain:6>);
recipes.addShaped(<IC2NuclearControl:blockNuclearControlMain:6>,
  [
    [<ore:plateIron>,<ore:circuitAdvanced>,<ore:plateIron>],
    [<ore:cableGt01Gold>,<IC2:blockElectric:4>,<ore:cableGt01Gold>]
  ]
);

//Range Upgrade
recipes.remove(<IC2NuclearControl:ItemUpgrade>);
recipes.addShaped(<IC2NuclearControl:ItemUpgrade>,
  [
    [<IC2:reactorCoolantSimple:1>,<IC2:reactorCoolantSimple:1>,<IC2:reactorCoolantSimple:1>],
    [<ore:cableGt01AnyCopper>,<IC2:itemFreq>.withTag({}),<ore:cableGt01AnyCopper>]
  ]
);

//text card
recipes.remove(<IC2NuclearControl:ItemTextCard>);
recipes.addShaped(<IC2NuclearControl:ItemTextCard>,
  [
    [null,<ore:circuitBasic>,null],
    [<minecraft:paper>,<ore:cableGt01Tin>,<minecraft:paper>],
    [null,<ore:circuitBasic>,null]
  ]
);

//monitor
recipes.remove(<IC2NuclearControl:remoteMonitor>);
recipes.addShaped(<IC2NuclearControl:remoteMonitor>,
  [
    [<ore:cableGt01Tin>,null,null],
    [<IC2:itemFreq>,<IC2NuclearControl:blockNuclearControlMain:5>,<IC2:itemFreq>],
    [<IC2NuclearControl:ItemUpgrade>,<ore:plateAlloyCarbon>,<ore:plateAlloyCarbon>]
  ]
);
